package com.AtomGamers.JonathanScripter.Minigroups;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class Main extends JavaPlugin implements Listener {
	private ArrayList<Team> times = new ArrayList<Team>();
	private static Main instance = null;
	public void onEnable() {
		instance = this;
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		this.getConfig().addDefault("Groups", new ArrayList<String>());
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		this.getLogger().info("Plugin Enabled");
		this.getCommand("mgadd").setExecutor(new MGAdd(false));
		this.getCommand("mgremove").setExecutor(new MGRemove(false));
		this.getCommand("mglist").setExecutor(new MGList(false));
	}
	public void loadGroups(){
		for (String s : this.getConfig().getStringList("Groups")) {
			LoadGroup(s);
			this.getLogger().info("Loaded Group " + s + ".");
		}		
	}
	public void onDisable() {
		this.getLogger().info("Plugin Disabled");
		instance = null;		
	}
	
	public static Main getInstance(){
		return Main.instance;
	}
	private final static ScoreboardManager sm = Bukkit.getScoreboardManager();
	private final static Scoreboard sb = sm.getNewScoreboard();

	public void LoadGroup(String group) {
		String Grupo = group.split(",")[0];
		String Tag = group.split(",")[1];
		ChatColor Color = ChatColor.valueOf(group.split(",")[2].toUpperCase());		
		Team time = sb.registerNewTeam(Grupo);
		time.setPrefix(Color+Tag);
		times.add(time);
	}
	public void removeGroup(String group){
		times.remove(sb.getTeam(group));
		sb.getTeam(group).unregister();
	}
}
