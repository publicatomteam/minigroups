package com.AtomGamers.JonathanScripter.Minigroups;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.AtomGamers.JonathanScripter.Minigroups.cmds.BaseCommand;

public class MGRemove extends BaseCommand {

	public MGRemove(boolean onlyRunByPlayer) {
		super(onlyRunByPlayer);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean playerCommandRun(Player p, Command command, String label,
			String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean run(CommandSender sender, Command command, String label,
			String[] args) {
		try{
			List<String> list = this.mainInstance().getConfig().getStringList("Groups");
			int x = 0;
			for(String s : list){
				if(s.startsWith(args[0]+",")){
					list.remove(x);
				}
				++x;
			}
			sender.sendMessage(ChatColor.GREEN+"Success. Group "+args[0]+" removed.");
			this.mainInstance().getConfig().set("Groups", list);
			this.mainInstance().saveConfig();
			
			return true;
		}catch(Exception e){
			sender.sendMessage(ChatColor.RED+"Error on remove a group.");
			return false;
		}
	}

}
