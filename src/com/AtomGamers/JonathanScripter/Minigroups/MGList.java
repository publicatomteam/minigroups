package com.AtomGamers.JonathanScripter.Minigroups;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.AtomGamers.JonathanScripter.Minigroups.cmds.BaseCommand;

public class MGList extends BaseCommand {

	public MGList(boolean onlyRunByPlayer) {
		super(onlyRunByPlayer);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean playerCommandRun(Player p, Command command, String label,
			String[] args) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean run(CommandSender sender, Command command, String label,
			String[] args) {
		try{
			List<String> list = this.mainInstance().getConfig().getStringList("Groups");
			sender.sendMessage("================GroupList================");
			for(String s : list){
				sender.sendMessage("Group: "+s.split(",")[0]+", Tag: "+s.split(",")[1]+", Color: "+s.split(",")[2]);	
			}
			sender.sendMessage("================GroupList================");
			sender.sendMessage(ChatColor.GREEN+"Success. Group "+args[0]+" added.");
			this.mainInstance().getConfig().set("Groups", list);
			this.mainInstance().saveConfig();
			return true;
		}catch(Exception e){
			sender.sendMessage(ChatColor.RED+"Error on add a group.");
			return false;
		}
	}

}
