package com.AtomGamers.JonathanScripter.Minigroups.cmds;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.AtomGamers.JonathanScripter.Minigroups.Main;

public abstract class BaseCommand implements CommandExecutor{
	
	private boolean onlyPlayer = false;
	private Main instance = Main.getInstance();
	
	public BaseCommand(boolean onlyRunByPlayer) {
		this.onlyPlayer = onlyRunByPlayer;
	}
		
	public final Main mainInstance(){
		return this.instance;
	}
	@Override
	public final boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if(this.onlyPlayer){
			if(sender instanceof Player){
				return this.playerCommandRun((Player) sender, command, label, args);
			}else{
				sender.sendMessage(ChatColor.RED+"You need be a player.");
				return true;
			}
		}
		return this.run(sender, command, label, args);
	}
	public abstract boolean playerCommandRun(Player p, Command command,
			String label, String[] args);
	public abstract boolean run(CommandSender sender, Command command,
			String label, String[] args);
	
}
